<?php

namespace App\Controller;

use App\Entity\Profil;
use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/comments")
 */
class CommentsController extends AbstractController
{
    /**
     * @Route("/", name="comments")
     */
    public function commentsAction(Request $request)
    {
        $error = '';
        try {
            $error = $request->get('error');
        } catch (\Exception $e){}

        $message = 'oui';
        try {
            $message = $request->get('message');
        } catch (\Exception $e){}

        $pane = 0;
        try {
            $pane = $request->get('pane');
        } catch (\Exception $e){}

        $unvalidatedComments = [];
        $validatedComments = [];
        $deniedComments = [];

        try {
            $unvalidatedComments = $this->getDoctrine()->getRepository(Comment::class)->findBy(array('validated'=>null));
            $validatedComments = $this->getDoctrine()->getRepository(Comment::class)->findBy(array('validated'=>true));
            $deniedComments = $this->getDoctrine()->getRepository(Comment::class)->findBy(array('validated'=>false));
        } catch (\Exception $e){
            $error = $e->getMessage();
        }

        return $this->render('security/gestion_comments.html.twig', [
            'unvalidatedComments' => $unvalidatedComments,
            'validatedComments' => $validatedComments,
            'deniedComments' => $deniedComments,
            'error' => $error,
            'pane' => $pane,
            'message' => $message,
        ]);
    }

    /**
     * @Route("/getcomments/{id}", name="getCommentForProfil")
     */
    public function getCommentsForProfilAction(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get("id");
        $profil = $this->getDoctrine()->getRepository(Profil::class)->findById($id);

        $comments = $this->getDoctrine()->getRepository(Comment::class)->findBy(array("profil"=>$profil));

        return $this->render('@App/Security/gestion_comments.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/acceptcomment/{id}", name="acceptComment")
     */
    public function acceptCommentsAction(Request $request, EntityManagerInterface $em)
    {
        $error = '';
        $pane = 0;
        try {
            $id = $request->get("id");

            $comment = $this->getDoctrine()->getRepository(Comment::class)->findOneBy(array("id"=>$id));

            if (!$comment) {
                throw $this->createNotFoundException(
                    'No comment found for id ' . $id
                );
            }

            $lastState = $comment->getValidated();
            $pane = 0;
            switch ($lastState){
                case 0 :
                    $pane = 3;
                    break;
                case 1 :
                    $pane = 2;
                    break;
                case null :
                    $pane = 1;
                    break;
                default :
                    $pane = 0;
            }

            $comment->setValidated(1);
            $em->flush();


        } catch (\Exception $e){
            $error = $e->getMessage();
        }
        return $this->redirectToRoute('comments', array('error'=> $error, 'pane' => $pane, 'message' => 'commentaire accepté'));
    }

    /**
     * @Route("/denycomment/{id}", name="denyComment")
     */
    public function denyCommentsAction(Request $request, EntityManagerInterface $em)
    {
        $error = '';
        $pane = 0;
        try {
            $id = $request->get("id");

            $comment = $this->getDoctrine()->getRepository(Comment::class)->findOneBy(array("id" => $id));

            if (!$comment) {
                throw $this->createNotFoundException(
                    'No comment found for id ' . $id
                );
            }
            $lastState = $comment->getValidated();
            switch ($lastState) {
                case 0 :
                    $pane = 3;
                    break;
                case 1 :
                    $pane = 2;
                    break;
                case null :
                    $pane = 1;
                    break;
                default :
                    $pane = 0;
            }
            $comment->setValidated(0);
            $em->flush();

        } catch (\Exception $e){
            $error = $e->getMessage();
        }
        return $this->redirectToRoute('comments', array('error' => $error, 'pane' => $pane, 'message' => 'commentaire refusé'));
    }
}
